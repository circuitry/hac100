#include <memory>
#include "payload.h"

class transport
{
public:
    void send(payload payload);
    std::vector<payload> receive();
};

std::shared_ptr<transport> make_transport();