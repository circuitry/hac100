#include <string>
#include <memory>

using payload = std::string;

struct take_pod
{
    uint8_t pod_number;
    bool high_priority;
};

std::vector<take_pod> parse_payload(std::vector<payload> payloads);